package com.api.fizzbuzz.execptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandler extends ResponseEntityExceptionHandler {

    /**
     * This method handle the bad request exceptions
     * @param ex this param indicate the exception variable
     * @param request this para indicate the request that is sending to the endpoint
     * @return return a response entity with the details of the error
     */
    @org.springframework.web.bind.annotation.ExceptionHandler({ResponseStatusException.class})
    public final ResponseEntity<Object> handleBadRequestException(ResponseStatusException ex, WebRequest request){
        ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, ex.getReason(), request.getDescription(false));
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }
}
