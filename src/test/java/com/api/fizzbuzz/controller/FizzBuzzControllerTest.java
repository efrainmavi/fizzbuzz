package com.api.fizzbuzz.controller;

import com.api.fizzbuzz.service.FizzBuzzService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.server.ResponseStatusException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest({FizzBuzzService.class, FizzBuzzController.class})
public class FizzBuzzControllerTest {

    public static final String TEST = "{\"Sequence\":[\"1\",\"2\",\"Fizz\",\"4\",\"Buzz\",\"Fizz\",\"7\",\"8\",\"Fizz\",\"Buzz\"]}";

    @Autowired
    private FizzBuzzController fizzBuzzController;

    @Autowired
    private FizzBuzzService fizzBuzzService;


    @Autowired
    private MockMvc mockMvc;

    /**
     * This test that endpoint is able to receive request and make a response
     *
     * Note: it can be considered as integration test
     * @throws Exception
     */
    @Test
    public void testRetrieveFizzBuzzSequence() throws Exception {
        MockHttpServletRequestBuilder getResult = MockMvcRequestBuilders.get("/api/v1/fizzbuzz/sequence/{value}", 1);
        getResult.contentType(MediaType.APPLICATION_JSON);
        MockMvcBuilders.standaloneSetup(this.fizzBuzzController)
                .build()
                .perform(getResult)
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    /**
     * This test the controller in order to be able to make a http request and get a success response
     *
     * Note: it can be considered as integration testing
     * @throws Exception
     */
    @Test
    public void retrieveFizzBuzzSeqControllerTest() throws Exception {
        String response = fizzBuzzService.convertToJson(10);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/v1/fizzbuzz/sequence/{value}", 10)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        String actualResult = mvcResult.getResponse().getContentAsString();

        assertEquals(response, actualResult);
    }

    /**
     * This test the convertToJson method in the service through the controller
     * and verify the response status code, it can be considered as Unit testing
     * @throws Exception
     */
    @Test
    public void retrieveFizzBuzzSeqServiceTest() throws Exception {

        FizzBuzzService service = Mockito.mock(FizzBuzzService.class);
        FizzBuzzController controller = new FizzBuzzController();

        when(service.convertToJson(any(Integer.class))).thenReturn(TEST);
        ReflectionTestUtils.setField(controller, "fizzBuzzService", service);
        ResponseEntity<Object> response = controller.retrieveFizzBuzzSequence(10);

        Mockito.verify(service).convertToJson(any(Integer.class));

        assertFalse(response.getStatusCode().isError());

    }

    /**
     * This test is for test when the api is throw a ResponseStatusException as bad request
     */
    @Test
    public void badRequestExceptionTest(){
        String msg = "Invalid value, the number should be greater than 0";
        assertThatThrownBy(() -> fizzBuzzController.retrieveFizzBuzzSequence(0))
                .isInstanceOf(ResponseStatusException.class)
                .hasMessageContaining(msg);

    }

}

