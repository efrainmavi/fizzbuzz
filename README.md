# FizzBuzz-rest-demo

The application provides a REST API that allows a basic operation:

    - Get FizzBuzz Sequence
    
The FizzBuzz follow the below specification:

    The application must have a single endpoint.
    This endpoint accepts a single parameter: the last element of the sequence.
    The return value should be the Fizz buzz sequence in JSON.
    Design a meaningful data structure.
    Implement some tests. The type of testing unit test and integration test

# Technical Description

The application was made on the Spring Initializr tool to create a skeleton
for Spring Boot app added the depencies:

    - Spring Web
        - Include Restful, Spring MVC
        - Apache Tomcat as embedded server, etc.
    - Spring Fox
        - Swagger Documentation API
        - Swagger UI
    - Jacoco Report
        
**Architectural pattern MVC**: This was chosen because it makes a perfect fit for this type of applications allowing to easily separate the view form all the logic behind.  
**Design Pattern Singleton**: By default spring beans and controllers are singleton, on an API that is receiving many calls this helps by reducing resources usage in the application, due only uses one instance of a class at a time.  
**List**: I choose this data structure due is easily to use due is not need to define the size another advantage is the lists are versatile and easy to manage.  
**Map**: I choose this data structure becasue I thought the match perfect to use with IntStream for calculate the sequence and the values stored them as string objects in a list, the second point as I was getting the sequence as a list of string object I decide to store that list in a map to have a key value `Sequence` and then have the list as values, the response it will show like {"Squence": ["1", "2", "Fizz" ...etc ]}  
**IntStream**: That returns an infinite sequential unordered stream where each element is generated by the provided IntSupplier. This allow me to use the rangeClose method defining a limit to stop the sequence this limit is provided by the user through the endpoint.  
**Jacoco Report**: I implement jacoco to generate a report in html and xml of the code coverage in the application when run the test and it can be checked in this path `build > reports > jacoco > test > html` or `build > reports > jacoco` and open the files `index.html` or  `jacocoTestReport.xml` to see the report.

# Usage

### Clone the repository

Run the following command to clone the repository using `ssh`:

    git clone git@gitlab.com:efrainmavi/fizzbuzz.git


Run the following command to clone the repository using `https`:

    git clone https://gitlab.com/efrainmavi/fizzbuzz.git

### How to start the application

To run the application simply run the `./gradlew bootRun` command on the command line at the root directory of the application or after to clone it run this command `./gradlew build` to create the build folder, then go to the build > libs through the command line using `cd` command to navigate in the directories then will find a jar with this name `java -jar fizzbuzz-0.0.1-SNAPSHOT.jar` and run it with the below command `java -jar <jar_name>`
   
   ### Interact with the application
   
   Once the application has successfully started should be available at [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html "http://localhost:8080/swagger-ui.html"), due is a swagger API documentation implemented which allows to interact with the API, from there the resource methods are expose at `/api/v1/fizzbuzzsequence/{value}`.
   
   #### Example
   
Retrieve FizzBuzz sequence

GET [http://localhost:8080/api/v1/fizzbuzz/sequence/{value}](http://localhost:8080/api/v1/fizzbuzz/sequence/{value})

Expected result:

```json
{
  "Sequence": [
    "1",
    "2",
    "Fizz",
    "4",
    "Buzz",
    "Fizz",
    "7",
    "8",
    "Fizz",
    "Buzz",
    "11",
    "Fizz",
    "13",
    "14",
    "FizzBuzz",
    "16"
  ]
}

```

   
   
   
   
   
   
   
   
   
   
   
   
   
