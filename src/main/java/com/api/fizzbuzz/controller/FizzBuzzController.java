package com.api.fizzbuzz.controller;

import com.api.fizzbuzz.service.FizzBuzzService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(FizzBuzzController.END_POINT)
public class FizzBuzzController {

    public static final String END_POINT = "/api/v1/fizzbuzz";

    @Autowired
    public FizzBuzzService fizzBuzzService;

    /**
     * This is the endpoint where is request for the FizzBuzz Sequence
     *
     * @param value: receive an Integer param which is coming in the URL
     * @return this method return a Http ResponseEntity
     */
    @GetMapping(value = "/sequence/{value}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> retrieveFizzBuzzSequence(@PathVariable("value") Integer value){
        if(value == 0){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid value, the number should be greater than 0");
        }
            String response = fizzBuzzService.convertToJson(value);
            return ResponseEntity.ok(response);
    }
}
