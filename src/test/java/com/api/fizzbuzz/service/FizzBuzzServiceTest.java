package com.api.fizzbuzz.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ContextConfiguration(classes = {FizzBuzzService.class})
@ExtendWith(SpringExtension.class)
public class FizzBuzzServiceTest {
    @Autowired
    private FizzBuzzService fizzBuzzService;

    /**
     * This test the method that calculate the FizzBuzz Sequence on the service class
     * UnitTest
     */
    @Test
    public void testFizzBuzzSequence() {
        List<String> actualFizzBuzzSequenceResult = this.fizzBuzzService.fizzBuzzSequence(10);
        List<String> expectedFizzBuzzSequenceResult = List.of("1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz");
        assertEquals(10, actualFizzBuzzSequenceResult.size());
        assertEquals(expectedFizzBuzzSequenceResult, actualFizzBuzzSequenceResult);
    }
    /**
     * This test the method that calculate the FizzBuzz Sequence on the service class
     * where the sequence is empty
     *
     * UnitTest
     */
    @Test
    public void testFizzBuzzSequence2() {
        assertTrue(this.fizzBuzzService.fizzBuzzSequence(0).isEmpty());
    }

    /**
     * This test the method that convert the returned sequence list from the FizzBuzzSequence method
     *
     */
    @Test
    public void testConvertToJson() {
        String expectedValue = "{\"Sequence\":[\"1\",\"2\",\"Fizz\",\"4\",\"Buzz\",\"Fizz\",\"7\",\"8\",\"Fizz\",\"Buzz\"]}";
        String actual = fizzBuzzService.convertToJson(10);

        assertEquals(expectedValue,actual);
    }
}

