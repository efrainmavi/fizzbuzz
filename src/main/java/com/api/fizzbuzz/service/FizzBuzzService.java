package com.api.fizzbuzz.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class FizzBuzzService {

    /**
     * This method calculate the FizzBuzz sequence
     * @param number: receive an Integer parameter that indicates the limit of the sequence
     * @return: this method return a list of strings
     */
    public List<String> fizzBuzzSequence(Integer number){
       return IntStream.rangeClosed(1, number)
                .mapToObj(i -> i % 3 == 0 ? (i % 5 == 0 ? "FizzBuzz" : "Fizz") : (i % 5 == 0 ? "Buzz" : i+""))
                .collect(Collectors.toList());
    }

    /**
     * This method convert the sequence list to Json
     * @param value: receive an Integer parameter which is coming from the controller
     *             and passed to the FizzBuzzSequence method
     * @return: this method return a String
     */
    public String convertToJson(Integer value){
        ObjectMapper mapper = new ObjectMapper();
        Map<String, Object> sequenceMap = new HashMap<>();
        String mapToJson = null;
        try {
            sequenceMap.put("Sequence", fizzBuzzSequence(value));
            mapToJson = mapper.writeValueAsString(sequenceMap);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return mapToJson;
    }
}
