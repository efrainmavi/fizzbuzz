package com.api.fizzbuzz.execptions;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ApiError {
    private HttpStatus status;
    private String message;
    private String path;

    public ApiError(HttpStatus status, String message, String path){
        super();
        this.status = status;
        this.message = message;
        this. path = path;
    }
}
